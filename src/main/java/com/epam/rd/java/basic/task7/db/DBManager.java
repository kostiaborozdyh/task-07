package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;



public class DBManager {
	private static final String URL = "jdbc:mysql://localhost/testdb";
	private static final String USER = "root";
	private static final String PASSWORD = "qazwsxedcA7?";
	private static final String FULL_URL = URL+"?"+"user="+USER+"&password="+PASSWORD;
	private static final String ID = "id";
	private static final String LOGIN = "login";
	private static final String NAME = "name";
	private static final String FIND_ALL_USERS = "SELECT * FROM users u ORDER BY u.id";
	private static final String FIND_USER = "SELECT * FROM users u WHERE u.login=?";
	private static final String FIND_TEAM = "SELECT * FROM teams t WHERE t.name=?";
	private static final String FIND_TEAM_ID = "SELECT * FROM teams t WHERE t.id=?";
	private static final String FIND_ALL_TEAMS = "SELECT * FROM teams t ORDER BY t.id";
	public static final String INSERT_USER = "INSERT INTO users(login) VALUES (?)";
	public static final String INSERT_TEAM = "INSERT INTO teams(name) VALUES (?)";
	public static final String INSERT_TEAMS_FOR_USERS = "INSERT INTO users_teams VALUES (?,?)";
	private static final String GET_USERS_TEAMS = "SELECT * FROM users_teams ut WHERE ut.user_id=?";
	private static final String DELETE_FROM_TEAMS = "DELETE FROM teams WHERE id =(?)";
	private static final String DELETE_FROM_USERS = "DELETE FROM users WHERE id =(?)";
	private static final String UPDATE = "UPDATE teams SET name=(?) WHERE id=(?)";
	private static final String	MESSAGE = "Connection failed...";
	public static final String TEAM_ID = "team_id";
	public static final String U;
	private static DBManager instance;
    private static final Properties properties = new Properties();
    static {
    	try{
    		properties.load(new FileReader("app.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
    	U = (String) properties.get("connection.url");
	}
	public static synchronized DBManager getInstance() {
		if(instance==null) instance = new DBManager();
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> listUser = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(FIND_ALL_USERS);
			ResultSet rs = st.executeQuery()) {
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt(ID));
				user.setLogin(rs.getString(LOGIN));
				listUser.add(user);
			}

		}catch(Exception ex){
			throw new DBException(MESSAGE,ex);
		}
		return listUser;
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection=null;
		PreparedStatement st=null;
		int count = 0;
		try{
			connection = DriverManager.getConnection(U);
			connection.setAutoCommit(false);
			st = connection.prepareStatement(INSERT_USER);
			st.setString(1,user.getLogin());
			 count = st.executeUpdate();
			 connection.commit();
		} catch (Exception e) {
			rollback(connection);
			throw new DBException(MESSAGE,e);
		}
		finally {
			close(connection);
			close(st);
		}
		return count>0;
	}




	public boolean deleteUsers(User... users) throws DBException {
		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(DELETE_FROM_USERS))
		{
			for (User user :
					users) {

				st.setInt(1,getUser(user.getLogin()).getId());
				st.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			throw new DBException(MESSAGE,e);
		}

	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(FIND_USER)) {
			st.setString(1,login);
			try(ResultSet rs = st.executeQuery()){
				while (rs.next()) {
					if (rs.getString(LOGIN).equals(login)) {
						user.setId(rs.getInt(ID));
						user.setLogin(rs.getString(LOGIN));
					}
				}
			}
		}catch(Exception ex){
			throw new DBException(MESSAGE,ex);
		}
		return user;

	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(FIND_TEAM)) {
			st.setString(1,name);
			try(ResultSet rs = st.executeQuery()){
				while (rs.next()) {
					if (rs.getString(NAME).equals(name)) {
						team.setId(rs.getInt(ID));
						team.setName(rs.getString(NAME));
					}
				}
			}
		}catch(Exception ex){
			throw new DBException(MESSAGE,ex);
		}
		return team;
	}
	public Team getTeam(int id) throws DBException {
		Team team = new Team();
		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(FIND_TEAM_ID)) {
			st.setInt(1,id);
			try(ResultSet rs = st.executeQuery()){
				while (rs.next()) {
					if (rs.getInt(ID)==id) {
						team.setId(rs.getInt(ID));
						team.setName(rs.getString(NAME));
					}
				}
			}
		}catch(Exception ex){
			throw new DBException(MESSAGE,ex);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> listTeam = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(FIND_ALL_TEAMS);
			ResultSet rs = st.executeQuery()) {
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt(ID));
				team.setName(rs.getString(NAME));
				listTeam.add(team);
			}

		}catch(Exception ex){
			throw new DBException(MESSAGE,ex);
		}
		return listTeam;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection=null;
		PreparedStatement st=null;
		int count = 0;
		try{
			connection = DriverManager.getConnection(U);
			connection.setAutoCommit(false);
			st = connection.prepareStatement(INSERT_TEAM,Statement.RETURN_GENERATED_KEYS);
			st.setString(1,team.getName());
			count = st.executeUpdate();
			try (ResultSet generatedKeys = st.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					team.setId(generatedKeys.getInt(1));
				}
			}
			connection.commit();
		} catch (Exception e) {
			rollback(connection);
			throw new DBException(MESSAGE,e);
		}
		finally {
			close(connection);
			close(st);
		}
		return count>0;
	}



	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection=null;
		PreparedStatement st=null;
		int res=0;
		try{
			connection = DriverManager.getConnection(U);
			connection.setAutoCommit(false);
			st = connection.prepareStatement(INSERT_TEAMS_FOR_USERS);
			for (Team team :
					teams) {
				int k=0;
				st.setInt(++k,getUser(user.getLogin()).getId());
				st.setInt(++k,getTeam(team.getName()).getId());
				st.executeUpdate();
			}
			connection.commit();
			res=1;
		}catch (Exception e) {
			rollback(connection);
			throw new DBException(MESSAGE,e);
		}
		finally {
			close(st);
			close(connection);
		}
		return res>0;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> listTeam = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(GET_USERS_TEAMS)) {
			st.setInt(1,getUser(user.getLogin()).getId());
			try(ResultSet rs = st.executeQuery()) {
				while (rs.next()) {
					int id = rs.getInt(TEAM_ID);
					listTeam.add(getTeam(id));
				}
			}

		}catch(Exception ex){
			throw new DBException(MESSAGE,ex.getCause());
		}
		return listTeam;
	}

	public boolean deleteTeam(Team team) throws DBException {

		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(DELETE_FROM_TEAMS)) {
			st.setInt(1,getTeam(team.getName()).getId());
			st.executeUpdate();
			return true;
		} catch (SQLException e) {
			throw new DBException(MESSAGE,e.getCause());
		}

	}

	public boolean updateTeam(Team team) throws DBException {
		try(Connection connection = DriverManager.getConnection(U);
			PreparedStatement st = connection.prepareStatement(UPDATE)) {
			int k=0;
			st.setString(++k,team.getName());
			st.setInt(++k,team.getId());
			st.executeUpdate();
		} catch (SQLException e) {
			throw new DBException(MESSAGE,e.getCause());
		}
		return false;

	}

	private void close(PreparedStatement st) {
		if(st !=null){
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void close(Connection connection) {
		if(connection !=null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	private void rollback(Connection connection) {
		try {
			connection.rollback();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
